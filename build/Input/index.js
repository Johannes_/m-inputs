'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.classes = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _mUiToolbox = require('m-ui-toolbox');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var classes = exports.classes = function classes(_ref, className, icon, modifier, size, hasChanges) {
  var error = _ref.error,
      submitFailed = _ref.submitFailed,
      dirty = _ref.dirty,
      touched = _ref.touched,
      warning = _ref.warning;

  var _classNames;

  return (0, _classnames2.default)('c-input', className, (_classNames = {
    'js-error': submitFailed && error,
    'js-dirty': dirty,
    'js-warning': touched && warning,
    'js-has-changes': hasChanges,
    'c-input_with-icon': icon
  }, _defineProperty(_classNames, 'c-input_' + modifier, modifier), _defineProperty(_classNames, 'c-input_size-' + size, size), _classNames));
};

var Input = function Input(props) {
  var name = props.name,
      placeholder = props.placeholder,
      className = props.className,
      intl = props.intl,
      _props$input = props.input,
      input = _props$input === undefined ? {} : _props$input,
      icon = props.icon,
      modifier = props.modifier,
      addOn = props.addOn,
      _props$type = props.type,
      type = _props$type === undefined ? 'text' : _props$type,
      _props$addOnPositionR = props.addOnPositionReversed,
      addOnPositionReversed = _props$addOnPositionR === undefined ? false : _props$addOnPositionR,
      _props$disabled = props.disabled,
      disabled = _props$disabled === undefined ? false : _props$disabled,
      _props$meta = props.meta,
      meta = _props$meta === undefined ? {} : _props$meta,
      hasChanges = props.hasChanges,
      maxLength = props.maxLength,
      autocomplete = props.autocomplete,
      size = props.size,
      min = props.min,
      max = props.max,
      step = props.step,
      initialValue = props.initialValue;


  var inputComponent = _react2.default.createElement('input', _extends({
    className: type === 'hidden' ? 'u-display-none' : classes(meta, className, icon, modifier, size, hasChanges),
    id: 'c-input-' + (input.name || name),
    placeholder: placeholder && placeholder.id ? intl.formatMessage(placeholder) : placeholder
  }, input, {
    name: input.name,
    type: type,
    min: min,
    max: max,
    step: step,
    disabled: disabled,
    onClick: props.onClick,
    maxLength: maxLength,
    autoComplete: autocomplete,
    value: input.value || initialValue
  }));

  if (type === 'hidden') return inputComponent;
  return _react2.default.createElement(
    _FormGroup2.default,
    props,
    icon && _react2.default.createElement(_mUiToolbox.Icon, {
      data: icon,
      className: 'c-input__icon',
      modifier: modifier,
      width: '13px',
      height: '13px'
    }),
    addOn ? _react2.default.createElement(
      _mUiToolbox.AddOn,
      { data: addOn, position: !addOnPositionReversed && 'left', modifier: modifier },
      inputComponent
    ) : inputComponent
  );
};

Input.propTypes = {
  className: _propTypes2.default.string,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object, _propTypes2.default.number, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  meta: _propTypes2.default.object,
  input: _propTypes2.default.object,
  name: _propTypes2.default.string,
  type: _propTypes2.default.string,
  initialValue: _propTypes2.default.string,
  autocomplete: _propTypes2.default.string,
  addOn: _propTypes2.default.string,
  maxLength: _propTypes2.default.number,
  intl: _reactIntl.intlShape.isRequired,
  icon: _propTypes2.default.node,
  disabled: _propTypes2.default.bool,
  hasChanges: _propTypes2.default.bool,
  addOnPositionReversed: _propTypes2.default.bool,
  onClick: _propTypes2.default.func,
  size: _propTypes2.default.oneOf(['large', 'full']),
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line', 'inline']),
  min: _propTypes2.default.number,
  max: _propTypes2.default.number,
  step: _propTypes2.default.number
};

exports.default = (0, _reactIntl.injectIntl)(Input);