'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

var _mUiToolbox = require('m-ui-toolbox');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var checkedIcon = function checkedIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/approved'
    ),
    _react2.default.createElement('path', {
      d: 'M11 4l-5.5 7L2 7',
      fill: 'none',
      fillRule: 'evenodd',
      strokeLinecap: 'round',
      strokeLinejoin: 'round'
    })
  );
};

checkedIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(_ref, className, size, theme, modifier, checked, readOnly) {
  var error = _ref.error,
      submitFailed = _ref.submitFailed,
      touched = _ref.touched,
      warning = _ref.warning;

  var _classNames;

  return (0, _classnames2.default)('c-checkbox', className, (_classNames = {
    'js-error': submitFailed && error,
    'js-checked': checked,
    'js-warning': touched && warning
  }, _defineProperty(_classNames, 'c-checkbox_size-' + size, size), _defineProperty(_classNames, 'c-checkbox_theme-' + theme, theme), _defineProperty(_classNames, 'c-checkbox_' + modifier, modifier), _defineProperty(_classNames, 'c-checkbox_read-only', readOnly), _classNames));
};

var Checkbox = function (_Component) {
  _inherits(Checkbox, _Component);

  function Checkbox() {
    _classCallCheck(this, Checkbox);

    return _possibleConstructorReturn(this, (Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).apply(this, arguments));
  }

  _createClass(Checkbox, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          input = _props.input,
          checked = _props.checked,
          onChange = _props.onChange;

      if (input && input.onChange) {
        input.onChange(checked || input.value || false);
      }
      if (onChange) {
        onChange(checked || input && input.value || false);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          className = _props2.className,
          _props2$input = _props2.input,
          input = _props2$input === undefined ? {} : _props2$input,
          modifier = _props2.modifier,
          size = _props2.size,
          theme = _props2.theme,
          label = _props2.label,
          intl = _props2.intl,
          _props2$nonBlocking = _props2.nonBlocking,
          nonBlocking = _props2$nonBlocking === undefined ? false : _props2$nonBlocking,
          _props2$readOnly = _props2.readOnly,
          readOnly = _props2$readOnly === undefined ? false : _props2$readOnly,
          formLabel = _props2.formLabel,
          _props2$checked = _props2.checked,
          checked = _props2$checked === undefined ? false : _props2$checked,
          _props2$meta = _props2.meta,
          meta = _props2$meta === undefined ? {} : _props2$meta,
          disabled = _props2.disabled;

      var inputValue = input.value || false;

      var _onClick = input.onClick,
          onChange = input.onChange,
          restInput = _objectWithoutProperties(input, ['onClick', 'onChange']);

      return _react2.default.createElement(
        _FormGroup2.default,
        _extends({}, this.props, {
          label: formLabel,
          showLabel: Boolean(formLabel),
          modifier: nonBlocking ? 'non-blocking' : modifier
        }),
        _react2.default.createElement(
          'label',
          {
            className: classes(meta, className, size, theme, modifier, checked || inputValue, readOnly),
            onClick: function onClick(e) {
              if (readOnly) {
                e.preventDefault();
              } else {
                if (_onClick) {
                  e.preventDefault();
                  _onClick(e, input);
                }
                if (onChange) {
                  onChange(e);
                }
              }
            }
          },
          _react2.default.createElement('input', _extends({}, restInput, {
            checked: checked || inputValue,
            readOnly: readOnly,
            type: 'checkbox',
            className: 'c-checkbox__input',
            disabled: input && input.disabled || disabled
          })),
          _react2.default.createElement(
            'span',
            { className: 'c-checkbox__visual' },
            _react2.default.createElement(_mUiToolbox.Icon, {
              className: 'c-checkbox__icon',
              modifier: modifier,
              data: checkedIcon,
              size: 'full'
            })
          ),
          _react2.default.createElement(
            'span',
            { className: 'c-checkbox__label' },
            label && label.id ? intl.formatMessage(label) : label
          )
        )
      );
    }
  }]);

  return Checkbox;
}(_react.Component);

Checkbox.propTypes = {
  intl: _reactIntl.intlShape.isRequired,
  className: _propTypes2.default.string,
  size: _propTypes2.default.string,
  theme: _propTypes2.default.string,
  label: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  formLabel: _propTypes2.default.string,
  meta: _propTypes2.default.object,
  input: _propTypes2.default.object,
  nonBlocking: _propTypes2.default.bool,
  readOnly: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  checked: _propTypes2.default.bool,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line'])
};

exports.default = (0, _reactIntl.injectIntl)(Checkbox);