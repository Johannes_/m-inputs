'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getScrollParents = exports.classes = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDatetime = require('react-datetime');

var _reactDatetime2 = _interopRequireDefault(_reactDatetime);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

require('moment/locale/nl');

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactIntl = require('react-intl');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _mUiToolbox = require('m-ui-toolbox');

var _Input = require('../Input');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /*eslint react/no-find-dom-node: "off"*/


// Components


// Utils


var calendarIcon = function calendarIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/calendar'
    ),
    _react2.default.createElement(
      'g',
      {
        fill: 'none',
        fillRule: 'evenodd'
      },
      _react2.default.createElement('path', {
        d: 'M2.509 1.5C1.399 1.5.5 2.4.5 3.492v7.016c0 1.1.902 1.992 2.009 1.992h7.982c1.11 0 2.009-.9 2.009-1.992V3.492c0-1.1-.902-1.992-2.009-1.992H2.51zM9.5 0v3M3.5 0v3M.5 6.5h12M.5 9.5h12M9.5 6.5v6M6.5 6.5v6M3.5 6.5v6'
      })
    )
  );
};

calendarIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};
var classes = exports.classes = function classes(_ref, className, modifier, position) {
  var submitFailed = _ref.submitFailed,
      error = _ref.error,
      dirty = _ref.dirty,
      touched = _ref.touched,
      warning = _ref.warning,
      hasChanges = _ref.hasChanges;

  var _classNames;

  return (0, _classnames2.default)('c-date-time-picker', className, (_classNames = {
    'js-error': submitFailed && error,
    'js-dirty': dirty,
    'js-warning': touched && warning,
    'js-has-changes': hasChanges
  }, _defineProperty(_classNames, 'c-date-time-picker-' + modifier, modifier), _defineProperty(_classNames, 'c-date-time-picker_position-' + position, position), _classNames));
};

// Input: DOM element
// Returns: { int offsetLeft, int offsetTop }
// Determines how much the parent components have scrolled to the top and left.
var getScrollParents = exports.getScrollParents = function getScrollParents(element) {
  var offsetLeft = 0;
  var offsetTop = 0;
  var currentElement = element;
  do {
    // eslint-disable-line
    if (currentElement.scrollLeft || currentElement.scrollTop) {
      offsetTop += currentElement.scrollTop;
      offsetLeft += currentElement.scrollLeft;
    }
  } while ((currentElement = currentElement.parentNode) !== null);
  return { offsetLeft: offsetLeft, offsetTop: offsetTop };
};

// This function calculates where the drop down can be placed. Needs a not fixed or absolute target element to work.
// It will try the left-bottom corner, the left-top corner and after it will move it to the left if it does not fit in view.
var determineDropDownPosition = function determineDropDownPosition(targetElement, targetDropDown) {
  // The drop down element is positioned Fixed, if the user scrolls the drop down element will stay in the original location.
  var targetElementNode = _reactDom2.default.findDOMNode(targetElement);
  var targetDropDownNode = _reactDom2.default.findDOMNode(targetDropDown);

  // To fix this we look at the parents and collect their scroll offsets.

  var _getScrollParents = getScrollParents(targetElementNode),
      offsetLeft = _getScrollParents.offsetLeft,
      offsetTop = _getScrollParents.offsetTop;

  // We also have to check if the drop down element is in the viewport and place it above the target field.


  if (targetDropDownNode.getBoundingClientRect().height + targetElementNode.getBoundingClientRect().height + targetElementNode.getBoundingClientRect().top > document.documentElement.clientHeight) {
    offsetTop = offsetTop + targetElementNode.getBoundingClientRect().height + targetDropDownNode.getBoundingClientRect().height;
  }

  // Also move the drop down element to the left if not all of the element is on the screen.
  var pickerRightBorderLocation = targetElementNode.getBoundingClientRect().left + targetDropDownNode.getBoundingClientRect().width;
  if (document.documentElement.clientWidth - pickerRightBorderLocation < 0) {
    offsetLeft -= document.documentElement.clientWidth - pickerRightBorderLocation;
  }

  return { offsetTop: offsetTop, offsetLeft: offsetLeft };
};

var DatePicker = function (_Component) {
  _inherits(DatePicker, _Component);

  function DatePicker() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, DatePicker);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).call.apply(_ref2, [this].concat(args))), _this), _this.handleChange = function (value) {
      var _this$props = _this.props,
          input = _this$props.input,
          showTime = _this$props.showTime,
          onlyTime = _this$props.onlyTime;

      var currentInput = input ? _moment2.default.isMoment(input.value) ? input.value : (0, _moment2.default)(input.value) : value;
      var nextInput = _moment2.default.isMoment(value) ? value : (0, _moment2.default)(value);
      // Check that the current and next value are not the same.
      if (currentInput !== nextInput && input && input.onChange) {
        // Then squash to an acceptable output with or without time included.
        if (onlyTime && (0, _moment2.default)(value, 'HH:mm', true).isValid()) {
          input.onChange((0, _moment2.default)(value, 'HH:mm', true).format('HH:mm'));
        } else if (onlyTime && (0, _moment2.default)(value, 'HH:mm:ss', true).isValid()) {
          input.onChange((0, _moment2.default)(value, 'HH:mm:ss', true).format('HH:mm:ss'));
        } else if (showTime && (0, _moment2.default)(value, 'YYYY-MM-DD HH:mm', true).isValid()) {
          input.onChange((0, _moment2.default)(value, 'YYYY-MM-DD HH:mm', true).format('YYYY-MM-DD HH:mm'));
        } else if (showTime && (0, _moment2.default)(value, 'YYYY-MM-DD HH:mm:ss', true).isValid()) {
          input.onChange((0, _moment2.default)(value, 'YYYY-MM-DD HH:mm:ss', true).format('YYYY-MM-DD HH:mm:ss'));
        } else if (!showTime && !onlyTime && (0, _moment2.default)(value, 'YYYY-MM-DD', true).isValid()) {
          input.onChange((0, _moment2.default)(value, 'YYYY-MM-DD', true).format('YYYY-MM-DD'));
        } else {
          input.onChange(value);
        }
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(DatePicker, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.meta && nextProps.meta.active) {
        // The picker is positioned Fixed, so if the user scrolls the location will be in the original location.
        var datePickerNode = _reactDom2.default.findDOMNode(this.datePicker);
        var pickerNodes = datePickerNode.getElementsByClassName('rdtPicker');
        var pickerNode = _reactDom2.default.findDOMNode(pickerNodes[0]);

        var _determineDropDownPos = determineDropDownPosition(this.datePicker, pickerNode),
            offsetTop = _determineDropDownPos.offsetTop,
            offsetLeft = _determineDropDownPos.offsetLeft;

        // Set the style fo the picker.


        pickerNode.setAttribute('style', 'margin-top: ' + (0 - offsetTop) + 'px; margin-left: ' + (0 - offsetLeft) + 'px;');
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          input = _props.input,
          modifier = _props.modifier,
          placeholder = _props.placeholder,
          meta = _props.meta,
          hasChanges = _props.hasChanges,
          size = _props.size,
          className = _props.className,
          icon = _props.icon,
          addOn = _props.addOn,
          showTime = _props.showTime,
          onlyTime = _props.onlyTime,
          disabled = _props.disabled,
          dateFormat = _props.dateFormat,
          position = _props.position,
          intl = _props.intl;


      var inputvalue = input.value;

      // The input is a string.
      // So check which format applies and only force it when it’s valid or typing will be impossible.
      if (showTime && !onlyTime) {
        if ((0, _moment2.default)(inputvalue, 'YYYY-MM-DD HH:mm', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'YYYY-MM-DD HH:mm');
        }
        if ((0, _moment2.default)(inputvalue, 'DD-MM-YYYY HH:mm', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'DD-MM-YYYY HH:mm');
        }
        if ((0, _moment2.default)(inputvalue, 'YYYY-MM-DD HH:mm:ss', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'YYYY-MM-DD HH:mm:ss');
        }
        if ((0, _moment2.default)(inputvalue, 'DD-MM-YYYY HH:mm:ss', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'DD-MM-YYYY HH:mm:ss');
        }
      } else if (onlyTime) {
        if ((0, _moment2.default)(inputvalue, 'HH:mm', true).isValid()) {
          inputvalue = (0, _moment2.default)((0, _moment2.default)().format('YYYY-MM-DD') + ' ' + inputvalue);
        }
      } else {
        if ((0, _moment2.default)(inputvalue, 'YYYY-MM-DD', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'YYYY-MM-DD');
        }
        if ((0, _moment2.default)(inputvalue, 'DD-MM-YYYY', true).isValid()) {
          inputvalue = (0, _moment2.default)(inputvalue, 'DD-MM-YYYY');
        }
      }

      var inputComponent = _react2.default.createElement(_reactDatetime2.default, _extends({
        inputProps: {
          name: input.name,
          className: (0, _Input.classes)(meta, className, icon, modifier, size, hasChanges),
          placeholder: placeholder && placeholder.id ? intl.formatMessage(placeholder) : placeholder,
          disabled: disabled,
          onContextMenu: function onContextMenu() {
            return _this2.datePicker.closeCalendar();
          }
        },
        ref: function ref(_ref3) {
          return _this2.datePicker = _ref3;
        },
        id: 'input-' + input.name,
        dateFormat: dateFormat,
        timeFormat: showTime
      }, input, {
        onChange: this.handleChange,
        onBlur: this.handleChange,
        value: inputvalue,
        className: classes(meta, className, modifier, position),
        viewMode: 'days',
        locale: 'NL'
      }));

      return _react2.default.createElement(
        _FormGroup2.default,
        this.props,
        icon && _react2.default.createElement(_mUiToolbox.Icon, {
          data: icon,
          modifier: modifier,
          className: 'c-input__icon',
          width: '13px',
          height: '13px'
        }),
        addOn ? _react2.default.createElement(
          _mUiToolbox.AddOn,
          { data: addOn, className: 'c-input__add-on' },
          inputComponent
        ) : inputComponent
      );
    }
  }]);

  return DatePicker;
}(_react.Component);

DatePicker.defaultProps = {
  addOn: null,
  className: '',
  dateFormat: 'DD-MM-YYYY',
  disabled: false,
  hasChanges: null,
  icon: calendarIcon,
  meta: {},
  input: {},
  modifier: null,
  onlyTime: false,
  placeholder: null,
  position: null,
  showTime: false
};

DatePicker.propTypes = {
  addOn: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  className: _propTypes2.default.string,
  dateFormat: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  hasChanges: _propTypes2.default.string,
  icon: _propTypes2.default.oneOfType([_propTypes2.default.node, _propTypes2.default.func]),
  input: _propTypes2.default.object.isRequired,
  meta: _propTypes2.default.object,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line']),
  onlyTime: _propTypes2.default.bool,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object, _propTypes2.default.number, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  position: _propTypes2.default.oneOf(['right']), // defaults to left.
  showTime: _propTypes2.default.bool
};

exports.default = (0, _reactIntl.injectIntl)(DatePicker);