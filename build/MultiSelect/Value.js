'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectValue = function SelectValue(_ref) {
  var value = _ref.value,
      children = _ref.children,
      onRemove = _ref.onRemove;
  return _react2.default.createElement(
    'div',
    {
      className: 'c-select-input__value',
      onClick: function onClick() {
        onRemove && onRemove(value);
      } // eslint-disable-line
    },
    value && value.value && _react2.default.createElement('i', { className: 'c-select-input__icon icon-' + value.value }),
    children,
    onRemove && '×'
  );
};

SelectValue.propTypes = {
  value: _propTypes2.default.object.isRequired,
  children: _propTypes2.default.node,
  onRemove: _propTypes2.default.func
};

exports.default = SelectValue;