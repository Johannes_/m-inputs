'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getClass = function getClass(_ref) {
  var className = _ref.className,
      isFocused = _ref.isFocused,
      isDisabled = _ref.isDisabled;
  return (0, _classnames2.default)('c-select-input__option', className, {
    'is-focus': isFocused,
    'is-disabled': isDisabled
  });
};

var getIconClass = function getIconClass(value) {
  return (0, _classnames2.default)('c-select-input__icon', _defineProperty({}, 'icon-' + value, value));
};

var AsyncSelectOption = function (_Component) {
  _inherits(AsyncSelectOption, _Component);

  function AsyncSelectOption() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, AsyncSelectOption);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = AsyncSelectOption.__proto__ || Object.getPrototypeOf(AsyncSelectOption)).call.apply(_ref2, [this].concat(args))), _this), _this.handleMouseDown = function (event) {
      _this.props.onSelect(_this.props.option, event);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(AsyncSelectOption, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          option = _props.option,
          isDisabled = _props.isDisabled;


      var content = _react2.default.createElement(
        'span',
        null,
        option.value && !option.value.value && _react2.default.createElement('i', { className: getIconClass(option.value) }),
        option.label && option.label.id ? _react2.default.createElement(_reactIntl.FormattedMessage, option.label) : option.label && option.label.value ? option.label.value : option.label
      );

      if (isDisabled) {
        return _react2.default.createElement(
          'div',
          { className: getClass(this.props) },
          _react2.default.createElement(
            'b',
            null,
            content
          )
        );
      }
      return _react2.default.createElement(
        'div',
        { className: getClass(this.props), onMouseDown: this.handleMouseDown },
        content
      );
    }
  }]);

  return AsyncSelectOption;
}(_react.Component);

AsyncSelectOption.propTypes = {
  onSelect: _propTypes2.default.func.isRequired,
  option: _propTypes2.default.object.isRequired,
  isDisabled: _propTypes2.default.bool
};

exports.default = AsyncSelectOption;