'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.constructOption = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

var _Option = require('./Option');

var _Option2 = _interopRequireDefault(_Option);

var _Value = require('./Value');

var _Value2 = _interopRequireDefault(_Value);

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _mUiToolbox = require('m-ui-toolbox');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


// Imports
var calendarIcon = function calendarIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement(
      'title',
      null,
      'icon/small/arrow-down'
    ),
    _react2.default.createElement('path', {
      d: 'M10.5 5l-4 4-4-4',
      fill: 'none',
      fillRule: 'evenodd'
    })
  );
};

calendarIcon.defaultProps = {
  width: '13',
  height: '13',
  viewBox: '0 0 13 13',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(showIcons, showFlags, modifier, disableDirty, _ref) {
  var error = _ref.error,
      submitFailed = _ref.submitFailed,
      dirty = _ref.dirty,
      touched = _ref.touched,
      warning = _ref.warning;
  return (0, _classnames2.default)('l-form-control', 'c-select-input', _defineProperty({
    'c-select-input--no-icons': !showIcons,
    'c-select-input--no-flags': !showFlags,
    'c-select-input--error': submitFailed && error,
    'js-error': submitFailed && error,
    'js-warning': touched && warning,
    'js-dirty': dirty && !disableDirty
  }, 'c-select-input_' + modifier, modifier));
};

var constructOption = exports.constructOption = function constructOption(id, name, options) {
  return _extends({
    value: id,
    label: name
  }, options);
};

var SelectMulti = function (_Component) {
  _inherits(SelectMulti, _Component);

  function SelectMulti(props) {
    _classCallCheck(this, SelectMulti);

    var _this = _possibleConstructorReturn(this, (SelectMulti.__proto__ || Object.getPrototypeOf(SelectMulti)).call(this, props));

    _this.handleOnChange = function (newValue) {
      var _this$props = _this.props,
          multi = _this$props.multi,
          input = _this$props.input,
          onChange = _this$props.onChange;


      var returnableValue = newValue ? !multi && newValue.value ? newValue.value : newValue : '';

      if (input && input.onChange) {
        input.onChange(returnableValue);
      }
      if (onChange) {
        onChange(returnableValue);
      }
      _this.setState({ currentValue: returnableValue });
    };

    _this.state = { currentValue: props.input && props.input.value || props.value || props.defaultValue };
    return _this;
  }

  _createClass(SelectMulti, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          options = _props.options,
          value = _props.value,
          noResultsText = _props.noResultsText,
          placeholder = _props.placeholder,
          _props$clearable = _props.clearable,
          clearable = _props$clearable === undefined ? false : _props$clearable,
          _props$showIcons = _props.showIcons,
          showIcons = _props$showIcons === undefined ? false : _props$showIcons,
          _props$showFlags = _props.showFlags,
          showFlags = _props$showFlags === undefined ? false : _props$showFlags,
          _props$disableDirty = _props.disableDirty,
          disableDirty = _props$disableDirty === undefined ? false : _props$disableDirty,
          _props$input = _props.input,
          input = _props$input === undefined ? {} : _props$input,
          _props$multi = _props.multi,
          multi = _props$multi === undefined ? false : _props$multi,
          modifier = _props.modifier,
          _props$disabled = _props.disabled,
          disabled = _props$disabled === undefined ? false : _props$disabled,
          _props$icon = _props.icon,
          icon = _props$icon === undefined ? calendarIcon : _props$icon,
          loadOptions = _props.loadOptions,
          _props$async = _props.async,
          async = _props$async === undefined ? false : _props$async,
          _props$onBlurResetsIn = _props.onBlurResetsInput,
          onBlurResetsInput = _props$onBlurResetsIn === undefined ? false : _props$onBlurResetsIn,
          _props$creatable = _props.creatable,
          creatable = _props$creatable === undefined ? false : _props$creatable,
          _props$valueComponent = _props.valueComponent,
          valueComponent = _props$valueComponent === undefined ? _Value2.default : _props$valueComponent,
          _props$optionComponen = _props.optionComponent,
          optionComponent = _props$optionComponen === undefined ? _Option2.default : _props$optionComponen,
          _props$promptTextCrea = _props.promptTextCreator,
          _promptTextCreator = _props$promptTextCrea === undefined ? 'Add option ' : _props$promptTextCrea,
          _props$meta = _props.meta,
          meta = _props$meta === undefined ? {} : _props$meta,
          autoload = _props.autoload,
          addOn = _props.addOn,
          intl = _props.intl,
          _props$cache = _props.cache,
          cache = _props$cache === undefined ? {} : _props$cache;

      var currentValue = this.state.currentValue;
      // Bam triple if statement, pretty = nope, works = √.
      // eslint-disable-next-line

      var SelectComponent = async ? creatable ? _reactSelect2.default.AsyncCreatable : _reactSelect2.default.Async : creatable ? _reactSelect2.default.Creatable : _reactSelect2.default;

      var inputComponent = _react2.default.createElement(SelectComponent, _extends({
        className: classes(showIcons, showFlags, modifier, disableDirty, meta),
        options: options,
        clearable: clearable,
        autosize: true,
        noResultsText: noResultsText && noResultsText.id ? intl.formatMessage(noResultsText) : noResultsText,
        placeholder: placeholder && placeholder.id ? intl.formatMessage(placeholder) : placeholder,
        optionComponent: optionComponent,
        valueComponent: valueComponent,
        backspaceToRemoveMessage: '',
        multi: multi,
        id: 'c-input-' + input.name
      }, input, {
        value: Array.isArray(input.value) && !multi ? undefined : input.value || currentValue,
        onBlur: function onBlur() {
          return input.onBlur && input.onBlur(value);
        },
        onChange: this.handleOnChange,
        disabled: disabled,
        loadOptions: loadOptions,
        cache: cache,
        promptTextCreator: function promptTextCreator(label) {
          return _promptTextCreator + ' ' + label;
        },
        onBlurResetsInput: onBlurResetsInput,
        autoload: autoload
      }));
      return _react2.default.createElement(
        _FormGroup2.default,
        this.props,
        icon && !meta.error && _react2.default.createElement(_mUiToolbox.Icon, {
          data: icon,
          modifier: modifier,
          className: 'c-input__icon',
          width: '13px',
          height: '13px'
        }),
        addOn ? _react2.default.createElement(
          _mUiToolbox.AddOn,
          {
            data: addOn,
            position: 'left',
            modifier: modifier,
            className: 'c-input__add-on'
          },
          inputComponent
        ) : inputComponent
      );
    }
  }]);

  return SelectMulti;
}(_react.Component);

SelectMulti.propTypes = {
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]),
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.array]),
  noResultsText: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.object, _propTypes2.default.string]),
  meta: _propTypes2.default.object,
  cache: _propTypes2.default.oneOfType([_propTypes2.default.bool, _propTypes2.default.object]),
  options: _propTypes2.default.array,
  icon: _propTypes2.default.node,
  valueComponent: _propTypes2.default.func,
  optionComponent: _propTypes2.default.func,
  onChange: _propTypes2.default.func,
  loadOptions: _propTypes2.default.func,
  clearable: _propTypes2.default.bool,
  async: _propTypes2.default.bool,
  autoload: _propTypes2.default.bool,
  onBlurResetsInput: _propTypes2.default.bool,
  creatable: _propTypes2.default.bool,
  promptTextCreator: _propTypes2.default.string,
  multi: _propTypes2.default.bool,
  showIcons: _propTypes2.default.bool,
  showFlags: _propTypes2.default.bool,
  intl: _reactIntl.intlShape.isRequired,
  input: _propTypes2.default.object,
  disabled: _propTypes2.default.bool,
  addOn: _propTypes2.default.string,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line'])
};

exports.default = (0, _reactIntl.injectIntl)(SelectMulti);