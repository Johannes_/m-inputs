'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

var _mUiToolbox = require('m-ui-toolbox');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Components


var classes = function classes(_ref, className, modifier) {
  var error = _ref.error,
      warning = _ref.warning,
      submitFailed = _ref.submitFailed,
      dirty = _ref.dirty;
  return (0, _classnames2.default)('c-textarea', className, _defineProperty({
    'js-error': submitFailed && error,
    'js-warning': error || warning,
    'js-dirty': dirty
  }, 'c-textarea_' + modifier, modifier));
};

var TextArea = function (_Component) {
  _inherits(TextArea, _Component);

  function TextArea() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, TextArea);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = TextArea.__proto__ || Object.getPrototypeOf(TextArea)).call.apply(_ref2, [this].concat(args))), _this), _this.state = {
      minHeight: 0
    }, _this.handleChange = function (e) {
      var input = _this.props.input;


      if (input.onChange) {
        input.onChange(e);
      }

      _this.setState({ minHeight: e.target.scrollHeight });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(TextArea, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          placeholder = _props.placeholder,
          className = _props.className,
          intl = _props.intl,
          input = _props.input,
          modifier = _props.modifier,
          meta = _props.meta,
          _props$disabled = _props.disabled,
          disabled = _props$disabled === undefined ? false : _props$disabled,
          icon = _props.icon,
          name = _props.name;
      var minHeight = this.state.minHeight;

      return _react2.default.createElement(
        _FormGroup2.default,
        this.props,
        icon && _react2.default.createElement(_mUiToolbox.Icon, {
          data: icon,
          modifier: modifier,
          className: 'c-input__icon c-input_error_icon',
          width: '13px',
          height: '13px'
        }),
        _react2.default.createElement('textarea', _extends({
          className: classes(meta, className, modifier),
          id: 'c-input-' + input.name,
          placeholder: placeholder && placeholder.id ? intl.formatMessage(placeholder) : placeholder
        }, input, {
          name: name,
          disabled: disabled,
          onChange: this.handleChange,
          style: { minHeight: minHeight < 960 ? minHeight : 960 }
        }))
      );
    }
  }]);

  return TextArea;
}(_react.Component);

TextArea.propTypes = {
  className: _propTypes2.default.string,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  meta: _propTypes2.default.shape({
    error: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
      id: _propTypes2.default.string.isRequired,
      defaultMessage: _propTypes2.default.string.isRequired
    }), _propTypes2.default.node]),
    warning: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
      id: _propTypes2.default.string.isRequired,
      defaultMessage: _propTypes2.default.string.isRequired
    })]),
    touched: _propTypes2.default.bool
  }),
  input: _propTypes2.default.object.isRequired,
  name: _propTypes2.default.string,
  intl: _reactIntl.intlShape.isRequired,
  modifier: _propTypes2.default.oneOf(['inverted', 'inline']),
  disabled: _propTypes2.default.bool
};

exports.default = (0, _reactIntl.injectIntl)(TextArea);