'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactIntl = require('react-intl');

var _mUiToolbox = require('m-ui-toolbox');

var _FormGroup = require('../FormGroup');

var _FormGroup2 = _interopRequireDefault(_FormGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Compnents


// Assets
var downArrowIcon = function downArrowIcon(props) {
  return _react2.default.createElement(
    'svg',
    props,
    _react2.default.createElement('path', {
      d: 'M13.157 6.936l-5.5 5.5-5.439-5.438',
      stroke: '#2B2B2B'
    })
  );
};

downArrowIcon.defaultProps = {
  width: '16',
  height: '16',
  viewBox: '0 0 16 16',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};


var classes = function classes(_ref, className, icon, modifier) {
  var error = _ref.error,
      submitFailed = _ref.submitFailed,
      dirty = _ref.dirty,
      touched = _ref.touched,
      warning = _ref.warning;
  return (0, _classnames2.default)('c-input', 'c-input_select', className, _defineProperty({
    'js-error': submitFailed && error,
    'js-warning': touched && warning,
    'js-dirty': dirty,
    'c-input_with-icon': icon
  }, 'c-input_' + modifier, modifier));
};

var Select = function Select(props) {
  var placeholder = props.placeholder,
      className = props.className,
      intl = props.intl,
      _props$input = props.input,
      input = _props$input === undefined ? {} : _props$input,
      addOn = props.addOn,
      _props$icon = props.icon,
      icon = _props$icon === undefined ? downArrowIcon : _props$icon,
      children = props.children,
      options = props.options,
      modifier = props.modifier,
      _props$multiple = props.multiple,
      multiple = _props$multiple === undefined ? false : _props$multiple,
      _props$disabled = props.disabled,
      disabled = _props$disabled === undefined ? false : _props$disabled,
      _props$meta = props.meta,
      meta = _props$meta === undefined ? {} : _props$meta,
      defaultValue = props.defaultValue,
      onChange = props.onChange,
      value = props.value,
      _props$disablePlaceho = props.disablePlaceholder,
      disablePlaceholder = _props$disablePlaceho === undefined ? false : _props$disablePlaceho;


  return _react2.default.createElement(
    _FormGroup2.default,
    props,
    icon && _react2.default.createElement(_mUiToolbox.Icon, {
      data: icon,
      modifier: modifier,
      className: 'c-input__icon',
      width: '13px',
      height: '13px'
    }),
    _react2.default.createElement(
      'select',
      _extends({
        multiple: multiple,
        className: classes(meta, className, icon, modifier),
        id: 'c-input-' + input.name,
        onChange: onChange
      }, input, {
        disabled: disabled,
        value: input.value || value || defaultValue
      }),
      placeholder && _react2.default.createElement(
        'option',
        { disabled: disablePlaceholder, value: '' },
        placeholder.id ? intl.formatMessage(placeholder) : placeholder
      ),
      options && options.length > 0 ? options.map(function (option, optionKey) {
        return _react2.default.createElement(
          'option',
          _defineProperty({
            value: option.value,
            selected: option.selected,
            disabled: option.disabled,
            key: 'input-select-' + input.name + '-' + optionKey
          }, 'disabled', option.disabled),
          option.title && option.title.id ? intl.formatMessage(option.title) : option.title
        );
      }) : children
    )
  );
};

Select.propTypes = {
  className: _propTypes2.default.string,
  placeholder: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  meta: _propTypes2.default.object,
  onChange: _propTypes2.default.func,
  input: _propTypes2.default.object,
  name: _propTypes2.default.string,
  addOn: _propTypes2.default.string,
  intl: _reactIntl.intlShape.isRequired,
  icon: _propTypes2.default.node,
  children: _propTypes2.default.node,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line']),
  disabled: _propTypes2.default.bool,
  disablePlaceholder: _propTypes2.default.bool,
  multiple: _propTypes2.default.bool,
  defaultValue: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
};

exports.default = (0, _reactIntl.injectIntl)(Select);