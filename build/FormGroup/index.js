'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactIntl = require('react-intl');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _hUtils = require('h-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Utils


var classes = function classes(wrapperClassName, modifier, _ref, disabled) {
  var _classNames;

  var error = _ref.error,
      warning = _ref.warning,
      submitFailed = _ref.submitFailed,
      dirty = _ref.dirty,
      active = _ref.active,
      touched = _ref.touched;
  return (0, _classnames2.default)('c-form-group', wrapperClassName, (_classNames = {}, _defineProperty(_classNames, 'c-form-group_' + modifier, modifier), _defineProperty(_classNames, 'js-error', submitFailed && error), _defineProperty(_classNames, 'js-warning', touched && warning), _defineProperty(_classNames, 'js-dirty', dirty), _defineProperty(_classNames, 'js-disabled', disabled), _defineProperty(_classNames, 'js-active', active), _classNames));
};

var getLabelClass = function getLabelClass(label, labelClassName) {
  return (0, _classnames2.default)('c-form-group__label', labelClassName, {
    'u-screenreader-only': label
  });
};

var FormGroup = function FormGroup(props) {
  var label = props.label,
      _props$meta = props.meta,
      meta = _props$meta === undefined ? {} : _props$meta,
      wrapperClassName = props.wrapperClassName,
      labelClassName = props.labelClassName,
      children = props.children,
      _props$showLabel = props.showLabel,
      showLabel = _props$showLabel === undefined ? true : _props$showLabel,
      modifier = props.modifier,
      intl = props.intl,
      disabled = props.disabled;


  var content = void 0;

  if (modifier !== 'table-cell' && meta && (meta.error || meta.warning)) {
    content = _react2.default.createElement(
      'span',
      null,
      meta.error && meta.submitFailed && _react2.default.createElement(
        'div',
        { className: 'c-form-group__error' },
        (0, _hUtils.getTranslation)(meta.error, intl)
      ),
      meta.warning && meta.touched && _react2.default.createElement(
        'div',
        { className: 'c-form-group__warning' },
        (0, _hUtils.getTranslation)(meta.warning, intl)
      )
    );
  }

  if (showLabel) {
    if (modifier == 'inline') {
      return _react2.default.createElement(
        'div',
        { className: classes(wrapperClassName, modifier, meta, disabled) },
        _react2.default.createElement(
          'label',
          { className: getLabelClass(label, labelClassName) },
          children,
          content,
          _react2.default.createElement(
            'div',
            { className: 'c-form-group__label-content' },
            label && (0, _hUtils.getTranslation)(label, intl)
          )
        )
      );
    }
    return _react2.default.createElement(
      'div',
      { className: classes(wrapperClassName, modifier, meta, disabled) },
      _react2.default.createElement(
        'label',
        { className: getLabelClass(label, labelClassName) },
        _react2.default.createElement(
          'div',
          { className: 'c-form-group__label-content' },
          label && (0, _hUtils.getTranslation)(label, intl)
        ),
        children,
        content
      )
    );
  }
  return _react2.default.createElement(
    'div',
    { className: classes(wrapperClassName, modifier, meta, disabled) },
    children,
    content
  );
};

FormGroup.propTypes = {
  children: _propTypes2.default.node.isRequired,
  labelClassName: _propTypes2.default.string,
  wrapperClassName: _propTypes2.default.string,
  showLabel: _propTypes2.default.bool,
  intl: _reactIntl.intlShape.isRequired,
  label: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.node, _propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    defaultMessage: _propTypes2.default.string.isRequired
  })]),
  meta: _propTypes2.default.object,
  modifier: _propTypes2.default.oneOf(['table-cell', 'one-line', 'inline'])
};

exports.default = (0, _reactIntl.injectIntl)(FormGroup);