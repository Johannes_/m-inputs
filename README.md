# UI Toolbox

## Note
This project is not inteded for production. Please use at your own volition.

## Install

```terminal
npm install git+https://bitbucket.org/Johannes_/m-ui-toolbox.git --save
```

Import SASS and compile this with webpack loader.

```sass
@import '../../node_modules/m-ui-toolbox/main';	
```

## Contributing

When you start developing you want to run the following command:

```terminal
npm run build:watch
```

* Include build assets; these and will be imported as a package
* Only add the relevant components to this library.
* Make sure it builds.
* Do not use external packages if this is not necessary.
* If you want to contribute don't forget to add the JS assets to the `src/index.js` and the SASS assets to `main.js`.
