import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';
import { AddOn, Icon } from 'm-ui-toolbox';

// Compnents
import FormGroup from '../FormGroup';

// Assets
import downArrowIcon from '../../assets/images/icons/md-down-arrow-minimal.svg';

const classes = ({ error, submitFailed, dirty, touched, warning }, className, icon, modifier) =>
  classNames('c-input', 'c-input_select', className, {
    'js-error': submitFailed && error,
    'js-warning': touched && warning,
    'js-dirty': dirty,
    'c-input_with-icon': icon,
    [`c-input_${modifier}`]: modifier
  });

const Select = props => {
  const {
    placeholder,
    className,
    intl,
    input = {},
    addOn,
    icon = downArrowIcon,
    children,
    options,
    modifier,
    multiple = false,
    disabled = false,
    meta = {},
    defaultValue,
    onChange,
    value,
    disablePlaceholder = false,
  } = props;

  return (
    <FormGroup {...props}>
      {icon && (
        <Icon
          data={icon}
          modifier={modifier}
          className="c-input__icon"
          width="13px"
          height="13px"
        />
      )}
      <select
        multiple={multiple}
        className={classes(meta, className, icon, modifier)}
        id={`c-input-${input.name}`}
        onChange={onChange}
        {...input}
        disabled={disabled}
        value={input.value || value || defaultValue}
      >
        {placeholder && (
          <option disabled={disablePlaceholder} value="">
            {placeholder.id ? intl.formatMessage(placeholder) : placeholder}
          </option>
        )}
        {options && options.length > 0
          ? options.map((option, optionKey) => (
              <option
                value={option.value}
                selected={option.selected}
                disabled={option.disabled}
                key={`input-select-${input.name}-${optionKey}`}
                disabled={option.disabled}
              >
                {option.title && option.title.id
                  ? intl.formatMessage(option.title)
                  : option.title}
              </option>
            ))
          : children}
      </select>
    </FormGroup>
  );
};

Select.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  meta: PropTypes.object,
  onChange: PropTypes.func,
  input: PropTypes.object,
  name: PropTypes.string,
  addOn: PropTypes.string,
  intl: intlShape.isRequired,
  icon: PropTypes.node,
  children: PropTypes.node,
  modifier: PropTypes.oneOf([
    'table-cell',
    'one-line'
  ]),
  disabled: PropTypes.bool,
  disablePlaceholder: PropTypes.bool,
  multiple: PropTypes.bool,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default injectIntl(Select);
