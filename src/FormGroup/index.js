import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';
import classNames from 'classnames';

// Utils
import { getTranslation } from 'h-utils';

const classes = (
  wrapperClassName,
  modifier,
  { error, warning, submitFailed, dirty, active, touched },
  disabled
) =>
  classNames('c-form-group', wrapperClassName, {
    [`c-form-group_${modifier}`]: modifier,
    'js-error': submitFailed && error,
    'js-warning': touched && warning,
    'js-dirty': dirty,
    'js-disabled': disabled,
    'js-active': active
  });

const getLabelClass = (label, labelClassName) =>
  classNames('c-form-group__label', labelClassName, {
    'u-screenreader-only': label
  });

const FormGroup = props => {
  const {
    label,
    meta = {},
    wrapperClassName,
    labelClassName,
    children,
    showLabel = true,
    modifier,
    intl,
    disabled
  } = props;

  let content;

  if (modifier !== 'table-cell' && meta && (meta.error || meta.warning)) {
    content = (
      <span>
        {meta.error &&
          meta.submitFailed && (
            <div className="c-form-group__error">
              {getTranslation(meta.error, intl)}
            </div>
          )}
        {meta.warning &&
          meta.touched && (
            <div className="c-form-group__warning">
              {getTranslation(meta.warning, intl)}
            </div>
          )}
      </span>
    );
  }

  if (showLabel) {
		if (modifier == 'inline') {
			return (
				<div className={classes(wrapperClassName, modifier, meta, disabled)}>
					<label className={getLabelClass(label, labelClassName)}>
						{children}
						{content}
						<div className="c-form-group__label-content">
							{label && getTranslation(label, intl)}
						</div>
					</label>
				</div>
			);
		}
    return (
      <div className={classes(wrapperClassName, modifier, meta, disabled)}>
        <label className={getLabelClass(label, labelClassName)}>
          <div className="c-form-group__label-content">
            {label && getTranslation(label, intl)}
          </div>
          {children}
          {content}
        </label>
      </div>
    );
  }
  return (
    <div className={classes(wrapperClassName, modifier, meta, disabled)}>
      {children}
      {content}
    </div>
  );
};

FormGroup.propTypes = {
  children: PropTypes.node.isRequired,
  labelClassName: PropTypes.string,
  wrapperClassName: PropTypes.string,
  showLabel: PropTypes.bool,
  intl: intlShape.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  meta: PropTypes.object,
  modifier: PropTypes.oneOf([
    'table-cell',
    'one-line',
    'inline'
  ])
};

export default injectIntl(FormGroup);
