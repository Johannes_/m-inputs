import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';

import { Icon } from 'm-ui-toolbox';
import FormGroup from '../FormGroup';
import checkedIcon from '../../assets/images/icons/approved.svg';

const classes = (
  { error, submitFailed, touched, warning },
  className,
  size,
  theme,
  modifier,
  checked,
  readOnly
) =>
  classNames('c-checkbox', className, {
    'js-error': submitFailed && error,
    'js-checked': checked,
    'js-warning': touched && warning,
    [`c-checkbox_size-${size}`]: size,
    [`c-checkbox_theme-${theme}`]: theme,
    [`c-checkbox_${modifier}`]: modifier,
    'c-checkbox_read-only': readOnly
  });

class Checkbox extends Component {
  componentDidMount() {
    const { input, checked, onChange } = this.props;
    if (input && input.onChange) {
      input.onChange(checked || input.value || false);
    }
    if (onChange) {
      onChange(checked || input && input.value || false);
    }
  }

  render() {
    const {
      className,
      input = {},
      modifier,
      size,
      theme,
      label,
      intl,
      nonBlocking = false,
      readOnly = false,
      formLabel,
      checked = false,
      meta = {},
      disabled
    } = this.props;
    const inputValue = input.value || false;
    const { onClick, onChange, ...restInput } = input;

    return (
      <FormGroup
        {...this.props}
        label={formLabel}
        showLabel={Boolean(formLabel)}
        modifier={nonBlocking ? 'non-blocking' : modifier}
      >
        <label
          className={classes(
            meta,
            className,
            size,
            theme,
            modifier,
            checked || inputValue,
            readOnly
          )}
          onClick={e => {
            if (readOnly) {
              e.preventDefault();
            } else {
              if (onClick) {
                e.preventDefault();
                onClick(e, input);
              }
              if (onChange) {
                onChange(e);
              }
            }
          }}
        >
          <input
            {...restInput}
            checked={checked || inputValue}
            readOnly={readOnly}
            type="checkbox"
            className="c-checkbox__input"
            disabled={input && input.disabled || disabled}
          />
          <span className="c-checkbox__visual">
            <Icon
              className="c-checkbox__icon"
              modifier={modifier}
              data={checkedIcon}
              size="full"
            />
          </span>
          <span className="c-checkbox__label">
            {label && label.id ? intl.formatMessage(label) : label}
          </span>
        </label>
      </FormGroup>
    );
  }
}

Checkbox.propTypes = {
  intl: intlShape.isRequired,
  className: PropTypes.string,
  size: PropTypes.string,
  theme: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  formLabel: PropTypes.string,
  meta: PropTypes.object,
  input: PropTypes.object,
  nonBlocking: PropTypes.bool,
  readOnly: PropTypes.bool,
  disabled: PropTypes.bool,
  checked: PropTypes.bool,
  modifier: PropTypes.oneOf(['table-cell', 'one-line'])
};

export default injectIntl(Checkbox);
