import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';

// Components
import FormGroup from '../FormGroup';
import { Icon } from 'm-ui-toolbox';


const classes = (
  { error, warning, submitFailed, dirty },
  className,
  modifier
) =>
  classNames('c-textarea', className, {
    'js-error': submitFailed && error,
    'js-warning': error || warning,
    'js-dirty': dirty,
    [`c-textarea_${modifier}`]: modifier
  });

class TextArea extends Component { 
  state = {
    minHeight: 0,
  }

  handleChange = (e) => {
    const { input } = this.props;

    if (input.onChange) {
      input.onChange(e);
    }

    this.setState({ minHeight: e.target.scrollHeight });
  }
  render() {
    const {
      placeholder,
      className,
      intl,
      input,
      modifier,
      meta,
      disabled = false,
      icon,
      name,
    } = this.props;
    const { minHeight } = this.state;
    return (
      <FormGroup {...this.props}>
        {icon && (
          <Icon
            data={icon}
            modifier={modifier}
            className="c-input__icon c-input_error_icon"
            width="13px"
            height="13px"
          />
        )}
        <textarea
          className={classes(meta, className, modifier)}
          id={`c-input-${input.name}`}
          placeholder={
            placeholder && placeholder.id
              ? intl.formatMessage(placeholder)
              : placeholder
          }
          {...input}
          name={name}
          disabled={disabled}
          onChange={this.handleChange}
          style={{ minHeight: minHeight < 960 ? minHeight : 960 }}
        />
      </FormGroup>
    );
  };
}
TextArea.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  meta: PropTypes.shape({
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired
      }),
      PropTypes.node
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired
      })
    ]),
    touched: PropTypes.bool
  }),
  input: PropTypes.object.isRequired,
  name: PropTypes.string,
  intl: intlShape.isRequired,
  modifier: PropTypes.oneOf(['inverted', 'inline']),
  disabled: PropTypes.bool
};

export default injectIntl(TextArea);
