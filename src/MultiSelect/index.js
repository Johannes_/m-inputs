import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import classNames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';

// Components
import SelectOption from './Option';
import SelectValue from './Value';
import FormGroup from '../FormGroup';
import { Icon, AddOn } from 'm-ui-toolbox';

// Imports
import calendarIcon from '../../assets/images/icons/arrow-down.svg';

const classes = (
  showIcons,
  showFlags,
  modifier,
  disableDirty,
  { error, submitFailed, dirty, touched, warning }
) =>
  classNames('l-form-control', 'c-select-input', {
    'c-select-input--no-icons': !showIcons,
    'c-select-input--no-flags': !showFlags,
    'c-select-input--error': submitFailed && error,
    'js-error': submitFailed && error,
    'js-warning': touched && warning,
    'js-dirty': dirty && !disableDirty,
    [`c-select-input_${modifier}`]: modifier
  });

export const constructOption = (id, name, options) => ({
  value: id,
  label: name,
  ...options
});

class SelectMulti extends Component {
  constructor(props) {
    super(props);
    this.state = { currentValue: props.input && props.input.value || props.value || props.defaultValue };
  }
  
  handleOnChange = newValue => {
    const { multi, input, onChange } = this.props;

    const returnableValue = newValue ?
      (!multi && newValue.value ? newValue.value : newValue) :
      '';
  
    if (input && input.onChange) {
      input.onChange(returnableValue);
    }
    if (onChange) {
      onChange(returnableValue);
    }
    this.setState({ currentValue: returnableValue });
  };

  render() {
    const {
      options,
      value,
      noResultsText,
      placeholder,
      clearable = false,
      showIcons = false,
      showFlags = false,
      disableDirty = false,
      input = {},
      multi = false,
      modifier,
      disabled = false,
      icon = calendarIcon,
      loadOptions,
      async = false,
      onBlurResetsInput = false,
      creatable = false,
      valueComponent = SelectValue,
      optionComponent = SelectOption,
      promptTextCreator = 'Add option ',
      meta = {},
      autoload,
      addOn,
      intl,
      cache = {}
    } = this.props;
    const { currentValue } = this.state;
    // Bam triple if statement, pretty = nope, works = √.
    // eslint-disable-next-line
    const SelectComponent = async
      ? creatable ? Select.AsyncCreatable : Select.Async
      : creatable ? Select.Creatable : Select;

      const inputComponent = (
      <SelectComponent
        className={classes(showIcons, showFlags, modifier, disableDirty, meta)}
        options={options}
        clearable={clearable}
        autosize
        noResultsText={
          noResultsText && noResultsText.id
            ? intl.formatMessage(noResultsText)
            : noResultsText
        }
        placeholder={
          placeholder && placeholder.id
            ? intl.formatMessage(placeholder)
            : placeholder
        }
        optionComponent={optionComponent}
        valueComponent={valueComponent}
        backspaceToRemoveMessage={''}
        multi={multi}
        id={`c-input-${input.name}`}
        {...input}
        value={Array.isArray(input.value) && !multi ? undefined : input.value || currentValue}
        onBlur={() => input.onBlur && input.onBlur(value)}
        onChange={this.handleOnChange}
        disabled={disabled}
        loadOptions={loadOptions}
        cache={cache}
        promptTextCreator={label => `${promptTextCreator} ${label}`}
        onBlurResetsInput={onBlurResetsInput}
        autoload={autoload}
      />
    );
    return (
      <FormGroup {...this.props}>
        {icon &&
          !meta.error && (
            <Icon
              data={icon}
              modifier={modifier}
              className="c-input__icon"
              width="13px"
              height="13px"
            />
          )}
        {addOn ? (
          <AddOn
            data={addOn}
            position="left"
            modifier={modifier}
            className="c-input__add-on"
          >
            {inputComponent}
          </AddOn>
        ) : (
          inputComponent
        )}
      </FormGroup>
    );
  }
}

SelectMulti.propTypes = {
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  noResultsText: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.object,
    PropTypes.string
  ]),
  meta: PropTypes.object,
  cache: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  options: PropTypes.array,
  icon: PropTypes.node,
  valueComponent: PropTypes.func,
  optionComponent: PropTypes.func,
  onChange: PropTypes.func,
  loadOptions: PropTypes.func,
  clearable: PropTypes.bool,
  async: PropTypes.bool,
  autoload: PropTypes.bool,
  onBlurResetsInput: PropTypes.bool,
  creatable: PropTypes.bool,
  promptTextCreator: PropTypes.string,
  multi: PropTypes.bool,
  showIcons: PropTypes.bool,
  showFlags: PropTypes.bool,
  intl: intlShape.isRequired,
  input: PropTypes.object,
  disabled: PropTypes.bool,
  addOn: PropTypes.string,
  modifier: PropTypes.oneOf([
    'table-cell',
    'one-line'
  ])
};

export default injectIntl(SelectMulti);
