import React, { Component } from 'react';
import PropTypes from 'prop-types';

const SelectValue = ({ value, children, onRemove }) => (
  <div
    className="c-select-input__value"
    onClick={() => {
      onRemove && onRemove(value);
    }} // eslint-disable-line
  >
    {value &&
      value.value && (
        <i className={`c-select-input__icon icon-${value.value}`} />
      )}
    {children}
    {onRemove && '×'}
  </div>
);

SelectValue.propTypes = {
  value: PropTypes.object.isRequired,
  children: PropTypes.node,
  onRemove: PropTypes.func
};

export default SelectValue;
