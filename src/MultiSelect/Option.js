import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

const getClass = ({ className, isFocused, isDisabled }) =>
  classNames('c-select-input__option', className, {
    'is-focus': isFocused,
    'is-disabled': isDisabled
  });

const getIconClass = value =>
  classNames('c-select-input__icon', {
    [`icon-${value}`]: value
  });

class AsyncSelectOption extends Component {
  handleMouseDown = (event) => {
    this.props.onSelect(this.props.option, event);
  }

  render() {
    const { option, isDisabled } = this.props;

    const content = (
      <span>
        {option.value &&
          !option.value.value && <i className={getIconClass(option.value)} />}
        {option.label && option.label.id ? (
          <FormattedMessage {...option.label} />
        ) : option.label && option.label.value ? (
          option.label.value
        ) : (
          option.label
        )}
      </span>
    );

    if (isDisabled) {
      return (
        <div className={getClass(this.props)}>
          <b>{content}</b>
        </div>
      );
    }
    return (
      <div className={getClass(this.props)} onMouseDown={this.handleMouseDown}>
        {content}
      </div>
    );
  }
}

AsyncSelectOption.propTypes = {
  onSelect: PropTypes.func.isRequired,
  option: PropTypes.object.isRequired,
  isDisabled: PropTypes.bool
};

export default AsyncSelectOption;
