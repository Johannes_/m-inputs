/*eslint react/no-find-dom-node: "off"*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDatePickerBootstrap from 'react-datetime';
import moment from 'moment';
import classNames from 'classnames';
import 'moment/locale/nl';
import ReactDOM from 'react-dom';
import { injectIntl } from 'react-intl';

// Components
import FormGroup from '../FormGroup';
import { Icon, AddOn } from 'm-ui-toolbox';

// Utils
import { classes as inputClasses } from '../Input';

import calendarIcon from '../../assets/images/icons/calendar.svg';

export const classes = ({ submitFailed, error, dirty, touched, warning, hasChanges }, className, modifier, position) =>
  classNames('c-date-time-picker', className, {
    'js-error': submitFailed && error,
    'js-dirty': dirty,
    'js-warning': touched && warning,
    'js-has-changes': hasChanges,
    [`c-date-time-picker-${modifier}`]: modifier,
    [`c-date-time-picker_position-${position}`]: position
  });

// Input: DOM element
// Returns: { int offsetLeft, int offsetTop }
// Determines how much the parent components have scrolled to the top and left.
export const getScrollParents = element => {
  let offsetLeft = 0;
  let offsetTop = 0;
  let currentElement = element;
  do {
    // eslint-disable-line
    if (currentElement.scrollLeft || currentElement.scrollTop) {
      offsetTop += currentElement.scrollTop;
      offsetLeft += currentElement.scrollLeft;
    }
  } while ((currentElement = currentElement.parentNode) !== null);
  return { offsetLeft, offsetTop };
};

// This function calculates where the drop down can be placed. Needs a not fixed or absolute target element to work.
// It will try the left-bottom corner, the left-top corner and after it will move it to the left if it does not fit in view.
const determineDropDownPosition = (targetElement, targetDropDown) => {
  // The drop down element is positioned Fixed, if the user scrolls the drop down element will stay in the original location.
  const targetElementNode = ReactDOM.findDOMNode(targetElement);
  const targetDropDownNode = ReactDOM.findDOMNode(targetDropDown);

  // To fix this we look at the parents and collect their scroll offsets.
  let { offsetLeft, offsetTop } = getScrollParents(targetElementNode);

  // We also have to check if the drop down element is in the viewport and place it above the target field.
  if (
    targetDropDownNode.getBoundingClientRect().height +
      targetElementNode.getBoundingClientRect().height +
      targetElementNode.getBoundingClientRect().top >
    document.documentElement.clientHeight
  ) {
    offsetTop =
      offsetTop +
      targetElementNode.getBoundingClientRect().height +
      targetDropDownNode.getBoundingClientRect().height;
  }

  // Also move the drop down element to the left if not all of the element is on the screen.
  const pickerRightBorderLocation =
    targetElementNode.getBoundingClientRect().left +
    targetDropDownNode.getBoundingClientRect().width;
  if (document.documentElement.clientWidth - pickerRightBorderLocation < 0) {
    offsetLeft -=
      document.documentElement.clientWidth - pickerRightBorderLocation;
  }

  return { offsetTop, offsetLeft };
};

class DatePicker extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.meta && nextProps.meta.active) {
      // The picker is positioned Fixed, so if the user scrolls the location will be in the original location.
      const datePickerNode = ReactDOM.findDOMNode(this.datePicker);
      const pickerNodes = datePickerNode.getElementsByClassName('rdtPicker');
      const pickerNode = ReactDOM.findDOMNode(pickerNodes[0]);

      const { offsetTop, offsetLeft } = determineDropDownPosition(
        this.datePicker,
        pickerNode
      );

      // Set the style fo the picker.
      pickerNode.setAttribute(
        'style',
        `margin-top: ${0 - offsetTop}px; margin-left: ${0 - offsetLeft}px;`
      );
    }
  }

  handleChange = (value) => {
    const { input, showTime, onlyTime } = this.props;
    const currentInput = input ? (
      moment.isMoment(input.value)
        ? input.value
        : moment(input.value)
      ) : value;
    const nextInput = moment.isMoment(value) ? value : moment(value);
    // Check that the current and next value are not the same.
    if (currentInput !== nextInput && input && input.onChange) {
      // Then squash to an acceptable output with or without time included.
      if (onlyTime && moment(value, 'HH:mm', true).isValid()) {
        input.onChange(moment(value, 'HH:mm', true).format(`HH:mm`));
      } else if (onlyTime && moment(value, 'HH:mm:ss', true).isValid()) {
        input.onChange(moment(value, 'HH:mm:ss', true).format(`HH:mm:ss`));
      } else if (
        showTime &&
        moment(value, 'YYYY-MM-DD HH:mm', true).isValid()
      ) {
        input.onChange(
          moment(value, 'YYYY-MM-DD HH:mm', true).format(`YYYY-MM-DD HH:mm`)
        );
      } else if (
        showTime &&
        moment(value, 'YYYY-MM-DD HH:mm:ss', true).isValid()
      ) {
        input.onChange(
          moment(value, 'YYYY-MM-DD HH:mm:ss', true).format(
            `YYYY-MM-DD HH:mm:ss`
          )
        );
      } else if (
        !showTime &&
        !onlyTime &&
        moment(value, 'YYYY-MM-DD', true).isValid()
      ) {
        input.onChange(moment(value, 'YYYY-MM-DD', true).format('YYYY-MM-DD'));
      } else {
        input.onChange(value);
      }
    }
  };

  render() {
    const {
      input,
      modifier,
      placeholder,
      meta,
      hasChanges,
      size,
      className,
      icon,
      addOn,
      showTime,
      onlyTime,
      disabled,
      dateFormat,
      position,
      intl
    } = this.props;

    let inputvalue = input.value;

    // The input is a string.
    // So check which format applies and only force it when it’s valid or typing will be impossible.
    if (showTime && !onlyTime) {
      if (moment(inputvalue, 'YYYY-MM-DD HH:mm', true).isValid()) {
        inputvalue = moment(inputvalue, 'YYYY-MM-DD HH:mm');
      }
      if (moment(inputvalue, 'DD-MM-YYYY HH:mm', true).isValid()) {
        inputvalue = moment(inputvalue, 'DD-MM-YYYY HH:mm');
      }
      if (moment(inputvalue, 'YYYY-MM-DD HH:mm:ss', true).isValid()) {
        inputvalue = moment(inputvalue, 'YYYY-MM-DD HH:mm:ss');
      }
      if (moment(inputvalue, 'DD-MM-YYYY HH:mm:ss', true).isValid()) {
        inputvalue = moment(inputvalue, 'DD-MM-YYYY HH:mm:ss');
      }
    } else if (onlyTime) {
      if (moment(inputvalue, 'HH:mm', true).isValid()) {
        inputvalue = moment(`${moment().format('YYYY-MM-DD')} ${inputvalue}`);
      }
    } else {
      if (moment(inputvalue, 'YYYY-MM-DD', true).isValid()) {
        inputvalue = moment(inputvalue, 'YYYY-MM-DD');
      }
      if (moment(inputvalue, 'DD-MM-YYYY', true).isValid()) {
        inputvalue = moment(inputvalue, 'DD-MM-YYYY');
      }
    }

    const inputComponent = (
      <ReactDatePickerBootstrap
        inputProps={{
          name: input.name,
          className: inputClasses(
            meta,
            className,
            icon,
            modifier,
            size,
            hasChanges
          ),
          placeholder:
            placeholder && placeholder.id
              ? intl.formatMessage(placeholder)
              : placeholder,
          disabled,
          onContextMenu: () => this.datePicker.closeCalendar()
        }}
        ref={ref => (this.datePicker = ref)}
        id={`input-${input.name}`}
        dateFormat={dateFormat}
        timeFormat={showTime}
        {...input}
        onChange={this.handleChange}
        onBlur={this.handleChange}
        value={inputvalue}
        className={classes(meta, className, modifier, position)}
        viewMode="days"
        locale="NL"
      />
    );

    return (
      <FormGroup {...this.props}>
        {icon && (
          <Icon
            data={icon}
            modifier={modifier}
            className="c-input__icon"
            width="13px"
            height="13px"
          />
        )}
        {addOn ? (
          <AddOn data={addOn} className="c-input__add-on">
            {inputComponent}
          </AddOn>
        ) : (
          inputComponent
        )}
      </FormGroup>
    );
  }
}

DatePicker.defaultProps = {
  addOn: null,
  className: '',
  dateFormat: 'DD-MM-YYYY',
  disabled: false,
  hasChanges: null,
  icon: calendarIcon,
  meta: {},
  input: {},
  modifier: null,
  onlyTime: false,
  placeholder: null,
  position: null,
  showTime: false
};

DatePicker.propTypes = {
  addOn: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  className: PropTypes.string,
  dateFormat: PropTypes.string,
  disabled: PropTypes.bool,
  hasChanges: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  input: PropTypes.object.isRequired,
  meta: PropTypes.object,
  modifier: PropTypes.oneOf(['table-cell', 'one-line']),
  onlyTime: PropTypes.bool,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.number,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  position: PropTypes.oneOf(['right']), // defaults to left.
  showTime: PropTypes.bool
};

export default injectIntl(DatePicker);
