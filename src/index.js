import CheckboxComponent from './Checkbox';
import DateTimeComponent from './DateTime';
import FormGroupComponent from './FormGroup';
import InputComponent from './Input';
import MultiSelectComponent from './MultiSelect';
import SelectComponent from './Select';
import TextAreaComponent from './TextArea';

export const Checkbox = CheckboxComponent;
export const DateTime = DateTimeComponent;
export const FormGroup = FormGroupComponent;
export const Input = InputComponent;
export const MultiSelect = MultiSelectComponent;
export const Select = SelectComponent;
export const TextArea = TextAreaComponent;
