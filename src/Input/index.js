import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { injectIntl, intlShape } from 'react-intl';
import FormGroup from '../FormGroup';
import { AddOn, Icon } from 'm-ui-toolbox';

export const classes = (
  { error, submitFailed, dirty, touched, warning },
  className,
  icon,
  modifier,
  size,
  hasChanges
) =>
  classNames('c-input', className, {
    'js-error': submitFailed && error,
    'js-dirty': dirty,
    'js-warning': touched && warning,
    'js-has-changes': hasChanges,
    'c-input_with-icon': icon,
    [`c-input_${modifier}`]: modifier,
    [`c-input_size-${size}`]: size
  });

const Input = props => {
  const {
    name,
    placeholder,
    className,
    intl,
    input = {},
    icon,
    modifier,
    addOn,
    type = 'text',
    addOnPositionReversed = false,
    disabled = false,
    meta = {},
    hasChanges,
    maxLength,
    autocomplete,
    size,
    min,
    max,
    step,
    initialValue
	} = props;
	
  const inputComponent = (
    <input
      className={
        type === 'hidden'
          ? 'u-display-none'
          : classes(meta, className, icon, modifier, size, hasChanges)
      }
      id={`c-input-${input.name || name}`}
      placeholder={
        placeholder && placeholder.id
          ? intl.formatMessage(placeholder)
          : placeholder
      }
      {...input}
      name={input.name}
      type={type}
      min={min}
      max={max}
      step={step}
      disabled={disabled}
      onClick={props.onClick}
      maxLength={maxLength}
      autoComplete={autocomplete}
      value={input.value || initialValue}
    />
  );

  if (type === 'hidden') return inputComponent;
  return (
    <FormGroup {...props}>
      {icon && (
        <Icon
          data={icon}
          className="c-input__icon"
          modifier={modifier}
          width="13px"
          height="13px"
        />
      )}
      {addOn ? (
        <AddOn data={addOn} position={!addOnPositionReversed && 'left'} modifier={modifier}>
          {inputComponent}
        </AddOn>
      ) : (
        inputComponent
      )}
    </FormGroup>
  );
};

Input.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.number,
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired
    })
  ]),
  meta: PropTypes.object,
  input: PropTypes.object,
  name: PropTypes.string,
  type: PropTypes.string,
  initialValue: PropTypes.string,
  autocomplete: PropTypes.string,
  addOn: PropTypes.string,
  maxLength: PropTypes.number,
  intl: intlShape.isRequired,
  icon: PropTypes.node,
  disabled: PropTypes.bool,
  hasChanges: PropTypes.bool,
  addOnPositionReversed: PropTypes.bool,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(['large', 'full']),
  modifier: PropTypes.oneOf([
    'table-cell',
    'one-line',
    'inline'
  ]),
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
};

export default injectIntl(Input);
